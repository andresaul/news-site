# Simple News App. 

Simple News application paste-on Symfony 4 Framework & Bootstrap

[![Build Status](https://travis-ci.org/coloso/symfony-docker.svg?branch=master)](https://travis-ci.org/coloso/symfony-docker)
### Prerequisites
* [Docker](https://www.docker.com/)

### Container
 - [nginx](https://hub.docker.com/_/nginx/) 1.16.+
 - [php-fpm](https://hub.docker.com/_/php/) 7.3.+
    - [composer](https://getcomposer.org/) 
    - [yarn](https://yarnpkg.com/lang/en/) and [node.js](https://nodejs.org/en/) (if you will use [Encore](https://symfony.com/doc/current/frontend/encore/installation.html) for managing JS and CSS)
- [mysql](https://hub.docker.com/_/mysql/) 5.7.+

### Installing

run docker and connect to container:
```
 docker-compose up --build
```
access to symfony console
```
 docker-compose exec php sh
```

install latest version of [Symfony](http://symfony.com/doc/current/setup.html) via composer:
```

configure the database connection information in your root directory `.env` 
```
DATABASE_URL=mysql://root:root@mysql:3306/symfony

migrate & load fixtures:

```
bin/console doctrine:migrations:migrate
bin/console doctrine:fixtures:load -n
```

install assets:
```
yarn install
yarn run encore dev
```

DONE :)

### Overview

App should available  localhost:80

For anonymous user is 3 pages
* The main page contains 3 most popular categories and the 3 news of each. 
 
   ![](images/main.png)
 
* News by category

    ![](images/by-category.png)

* Article view with comments

    ![](images/news.png)   

For back-office first signup or use user "web@mail.test" pass "testapp". There can edit/delete everything

