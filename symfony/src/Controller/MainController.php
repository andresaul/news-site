<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\News;
use App\Form\CommentPublicType;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class MainController extends AbstractController
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;
    private $categoryRepository;

    public function __construct(
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->newsRepository = $newsRepository;
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * @Route("/", name="main", methods={"GET"})
     */
    public function main(
        NewsRepository $newsRepository,
        Request $request
    ): Response {
        $categories = $this->categoryRepository->findMostPopular(3);

        return $this->render('public/main.html.twig', [
            'categories' => $categories,
        ]);
    }


    /**
     * @Route("/news-by-category/{id}", name="newsByCategory", methods={"GET"})
     */
    public function newsByCategory(
        Category $category,
        Request $request,
        PaginatorInterface $paginator
    ): Response {
        $news = $category->getNews();
        $pagination = $paginator->paginate(
            $news,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('public/category.html.twig', [
            'pagination' => $pagination,
            'category'   => $category,
        ]);
    }


    /**
     * @Route("/news/{id}", name="publicNews", methods={"GET"})
     */
    public function publicNews(News $news, Request $request): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentPublicType::class, $comment,
            ['news_id' => $news->getId()]);

        return $this->render('public/news.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/comment/add/{id}", name="addComment", methods={"POST"})
     */
    public function addComment(News $news, Request $request): Response
    {
        $form = $this->createForm(CommentPublicType::class);
        $this->createFormBuilder();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $comment = new Comment();
            $comment->setNews($news);
            $comment->setContent($form->getData()->getContent());
            $comment->setOwner($form->getData()->getOwner());

            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('publicNews',
                array('id' => $news->getId()));
        }
    }
}

