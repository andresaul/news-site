<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;
    private $categoryRepository;

    public function __construct(
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->newsRepository = $newsRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/dashboard", name="dashboard", methods={"GET"})
     */
    public function dashboard(CategoryRepository $categoryRepository): Response
    {
        return $this->render('admin/dashboard.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }
}
