<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $lipsum = new LoremIpsum();

        $randomPics[] = '439228hd738t6.jpg';
        $randomPics[] = '439419hbc78t69.jpg';
        $randomPics[] = '439216h463ft66.jpg';
        $randomPics[] = '439214hb4cct66.jpg';
        $randomPics[] = '624505hdc7bt65.jpg';

        $user = new User();
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEmail('web@mail.test');
        $p
            = '$argon2id$v=19$m=65536,t=4,p=1$U2VxRU8yZWhGT3dMTVJKSA$pCYZgqpm5JlNbwhURWu5718dIcC1nhcMwLdoDeTO/sI'; // testapp
        $user->setPassword($p);
        $manager->persist($user);

        for ($i = 0; $i < rand(10, 20); $i++) {
            $category = new Category();
            $category->setTitle('Category '.$lipsum->words(1));
            $categories[] = $category;
        }

        for ($j = 0; $j < rand(1000, 3000); $j++) {
            $news = new News();
            $news->setTitle($lipsum->words(rand(1, 3)));
            $news->setShortDescription($lipsum->sentences(rand(1, 2)));
            $news->setContent($lipsum->paragraphs(2));
            $news->setImageName($randomPics[array_rand($randomPics)]);
            $news->setImageSize(0);
            $datetime = new DateTime();
            $datetime->modify('-'.rand(1, 1000).' day');
            $news->setCreatedAt($datetime);

            $category = $categories[rand(0, $i - 1)];

            $news->addCategory($category);
            $category->addNews($news);
            $manager->persist($news);

            for ($k = 0; $k < rand(0, 20); $k++) {
                $comment = new Comment();
                $comment->setContent('comment '.$lipsum->words(rand(1, 200)));
                $comment->setNews($news);
                $comment->setOwner('Owner '.$lipsum->words(rand(1, 2)));
                $manager->persist($comment);
            }
        }


        $manager->flush();
    }
}
