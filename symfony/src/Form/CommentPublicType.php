<?php

namespace App\Form;

use App\Entity\Comment;
use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentPublicType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'attr' => array(
                    'class'       => 'form-control',
                    'placeholder' => 'write a comment...',
                    'rows'        => 5,
                ),
            ])
            ->add('owner', TextType::class, [
                'attr' => array(
                    'class'       => 'form-control',
                    'placeholder' => 'Your name',
                ),
            ])
            ->add('news_id', HiddenType::class, array(
                'data' => $options['news_id'],
            ));

        $builder->add('Post', SubmitType::class, [
            'attr' => ['class' => 'btn btn-info pull-right'],
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
            'news_id'    => null,
        ]);
    }
}
